﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnMover : MonoBehaviour 
{
    private float speed = 0.5f;
	
	// Update is called once per frame
	void Update () 
	{
        if(Time.timeSinceLevelLoad > 2.0f)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, 0.0f, transform.position.z), speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, new Vector3(transform.position.x, 0.0f, transform.position.z)) < 0.01)
            {
                GameManager.GameStarted = true;
                this.enabled = false;
            }
            speed += Time.deltaTime;
        }
    }
}
