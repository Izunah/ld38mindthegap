﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{
    public float baseSpeed = 10.0f;
    public float freezeMoveTime = 0.5f;

    public float rotateSpeed = 50.0f;
    public float grappleFiringSpdMod = 0.5f; //Modify our base speed by this number while firing our grapple
    public float grappleCrossingSpdMod = 5.0f; //How fast we travel when we cross the gap

    private Rigidbody rb;
    private Vector3 movement;
    private AudioSource playerAudio;

    private GameObject enemyTarget;
    private GameObject gameManager;
    private Transform crossingTarget;

    private EnemyManager emScript;
    private GameManager gmScript;

    private int floorMask;
    private int ourSide = 1; //Default to the first side

    private float currentSpeed = 10.0f;
    private float freezeTimer = 0.0f;

    private float moveHor = 0.0f;
    private float moveVer = 0.0f;

    private bool freezeMove = false;
    private bool playerCrossing = false;
    private bool swapped = false;
    public bool destroyedEnemy = false;

    private void Start()
    {
        currentSpeed = baseSpeed;
        rb = GetComponent<Rigidbody>();
        playerAudio = gameObject.GetComponent<AudioSource>();
        floorMask = LayerMask.GetMask("Floor");

        gameManager = GameObject.FindGameObjectWithTag("GameManager");

        if (gameManager != null)
        {
            emScript = gameManager.GetComponent<EnemyManager>();
            gmScript = gameManager.GetComponent<GameManager>();
        }
    }

    private void Update()
    {
        RotatePlayer();

        if (ourSide == 1)
            swapped = false;
        else
            swapped = true;

        destroyedEnemy = false;

        if (Input.GetButtonDown("Fire1"))
        {
            if (enemyTarget != null)
            {

                if(enemyTarget.GetComponent<EnemyBehaviour>().IsVulnerable())
                {
                    playerAudio.Play();
                    emScript.Death(enemyTarget);
                    enemyTarget = null;
                    freezeMove = false;
                    destroyedEnemy = true;
                }
            }
        }

        if (freezeMove && freezeTimer < Time.time)
        {
            if(enemyTarget != null && !destroyedEnemy)
            {
                destroyedEnemy = false;
                gmScript.AddToMultiplier(2.0f);
            }

            freezeMove = false;
            enemyTarget = null;
            destroyedEnemy = false;
        }
    }

    private void FixedUpdate()
    {
        if(playerCrossing)
        {
            Vector3 returnDir = transform.position - crossingTarget.transform.position; //Calculate the angle vector we need to return the hook towards.
            rb.MovePosition(transform.position - (returnDir.normalized * currentSpeed * Time.deltaTime));
        }

        if(!freezeMove && !playerCrossing)
        {
            moveHor = Input.GetAxisRaw("Horizontal");
            moveVer = Input.GetAxisRaw("Vertical");

            if (swapped)
                movement.Set(-moveHor, 0, -moveVer);
            else
                movement.Set(moveHor, 0, moveVer);

            rb.MovePosition(transform.position + (movement.normalized * currentSpeed * Time.deltaTime));
        }
    }

    private void RotatePlayer()
    {
         Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
         RaycastHit camRayHit;
         float camRayLength = 5000.0f;

         if (Physics.Raycast(camRay, out camRayHit, camRayLength, floorMask))
         {
             Vector3 lookDir = camRayHit.point - transform.position;
             lookDir.y = 0.0f;
             rb.MoveRotation(Quaternion.LookRotation(lookDir));
         }
    }
    private void SetFreezeMove()
    {
        freezeTimer = Time.time + freezeMoveTime;
        freezeMove = true;
    }
    public int GetSide()
    {
        return ourSide;
    }

    public void GrappleFired()
    {
        currentSpeed *= grappleFiringSpdMod;
    }

    public void GrappleReturned()
    {
        currentSpeed = baseSpeed;
    }

    public void TowedEnemy(GameObject enemy)
    {
        SetFreezeMove();
        enemyTarget = enemy;
    }

    public IEnumerator CrossSide(Transform target)
    {
        EnemyBehaviour ebScript = target.GetComponent<EnemyBehaviour>(); //Grab a reference for later use a couple times.
        enemyTarget = target.gameObject; //Set our players current target to this one, since we'll need that to destroy it later

        playerCrossing = true;
        rb.AddForce(Vector3.up * 10, ForceMode.VelocityChange); //Give us a little jump **DISABLED POTENTIAL DUE TO NAVMESH ERRORS!**

        crossingTarget = target;
        currentSpeed = baseSpeed * grappleCrossingSpdMod;
        Camera.main.transform.GetComponentInParent<CameraFollow>().PlayerSwitchedSides();
        
        while (Vector3.Distance(transform.position, target.position) > 2)
        {
            yield return null;
        }

        SetFreezeMove();
        playerCrossing = false;
        currentSpeed = baseSpeed;

        ourSide = ebScript.GetSide(); // Set our new side to that of our enemy, because we know that side should be right.
        ebScript.Vulnerable();

        emScript.PlayerSwappedSides(ourSide); //Let the enemy manager know we have successful swapped sides and these enemies can now follow us.
        gameObject.GetComponentInChildren<Grapple>().PlayerCrossed();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Boundary"))
        {
            gameObject.GetComponent<PlayerHealth>().Death();
        }
    }

    public bool IsJumping()
    {
        return playerCrossing;
    }
}