﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour 
{
    public int baseHealth = 100;
    public GameObject playerDeathParticles;

    private int currentHealth = 100;
    private GameManager gmScript;
    private EnemyManager emScript;

	// Use this for initialization
	void Start () 
	{
        gmScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        emScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<EnemyManager>();

        currentHealth = baseHealth;
        gmScript.UpdateHealth(currentHealth);
	}
	
    public void DealDamage(int damage)
    {
        Mathf.Max(currentHealth -= damage, 0);
        gmScript.UpdateHealth(currentHealth);

        if (currentHealth == 0)
        {
            Death();
        }
    }

    public void Death()
    {
        Camera.main.gameObject.GetComponentInParent<CameraFollow>().enabled = false;
        Vector3 spawnPos = transform.position;
        spawnPos.y += 1.5f;
        Instantiate(playerDeathParticles, spawnPos, Quaternion.Euler(-90, 0, 0));
        emScript.PlayerDied();
        Destroy(gameObject);
    }
}