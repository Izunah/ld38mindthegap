﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public int whichSide = 2; //Set on spawn, 2 is a default testing value set to private on release

    public int score = 10;
    public int power = 10; //How much damage do we do?

    public float ourSpeed = 4.0f;
    public float walkDistance = 10.0f; //How far do we walk?

    public float idleTime = 1.5f; //How often will we find a new idle position
    public float spawnTime = 1.0f;
    public float vulnerableTime = 0.5f;
    public float pursuePlayerDelay = 1.0f;

    public float attackRate = 0.5f;

    private float spawnTimer = 0.0f;
    private float idleTimer = 0.0f;
    private float vulnerableTimer = 0.0f;
    private float pursuePlayerTimer = 0.0f;
    private float attackTimer = 0.0f;

    public enum EnemyState { Null, Spawn, Idle, Walking, Follow, Grappled, Vulnerable };
    public EnemyState currentState = EnemyState.Null;

    private GameObject player; //This will generally be our target;
    private PlayerHealth phScript;

    private Vector3 idleDestination;
    private EnemyManager emScript;

    private Rigidbody rb;
    private CapsuleCollider ourCol;
    private MeshRenderer ourMesh;
    private AudioSource ourAudio;
    private Color defaultColor;

    private bool playerAttackable = false;

    //Boundaries used to clamp the enemies movement while idling
    private float xBoundary = 18.5f;
    private float zFarBoundary = 29.5f;
    private float zCloseBoundary = 8.5f;

    public IEnumerator SpawnInWorld()
    {
        yield return new WaitForSeconds(1.0f);

        currentState = EnemyState.Spawn;
        spawnTimer = Time.time + Random.Range(spawnTime / 2, spawnTime);

        while (currentState == EnemyState.Spawn)
            yield return null;

        
        gameObject.GetComponentInParent<SpawnTileMover>().AscendTile();
        transform.SetParent(null);

        if (player != null)
        {
            PlayerController pcScript = player.GetComponent<PlayerController>();
            phScript = player.GetComponent<PlayerHealth>();

            if (pcScript.GetSide() == whichSide && !pcScript.IsJumping())
            {
                pursuePlayerTimer = Time.time + pursuePlayerDelay;
            }
        }

        currentState = EnemyState.Idle;

        yield return null;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        ourCol = GetComponent<CapsuleCollider>();
        ourMesh = GetComponent<MeshRenderer>();
        ourAudio = GetComponent<AudioSource>();

        defaultColor = ourMesh.material.color;

        player = GameObject.FindGameObjectWithTag("Player");

        emScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<EnemyManager>();
    }

    private void FixedUpdate()
    { //Handle any form of movement here to take advantage of the physics system
        if(currentState == EnemyState.Spawn)
            rb.MovePosition(transform.position + (transform.forward * (Random.Range(ourSpeed * 1.5f, ourSpeed * 3.0f)) * Time.deltaTime));

        if (currentState == EnemyState.Follow)
        { //Follow the player around if we are his puppet
            AdjustGrappledVariables(false);//Safety Call

            if (player != null)
            {
                if (!player.GetComponent<PlayerController>().IsJumping())
                {//If the player is not jumping.
                    Vector3 direction = player.transform.position - transform.position;
                    rb.MovePosition(transform.position + (direction.normalized * ourSpeed * Time.deltaTime));
                }
            }
        }

        if(currentState == EnemyState.Walking)
        { //While our state is Walking, move towards a random destination that we found while we were idling.
            Vector3 direction = idleDestination - transform.position;
            rb.MovePosition(transform.position + (direction.normalized * ourSpeed * Time.deltaTime));

            if (Vector3.Distance(transform.position, idleDestination) < 2.0f)
            { //If we have reached a spot reasonable close to our detination, lets idle, wait, and find a new destination
                currentState = EnemyState.Idle;
                idleTimer = Time.time + idleTime;
            }
        }
    }
    private Vector3 CalculateRandomDestination()
    { //Find ourselves a random destination within the bounds of the side that we are on.
        float xPos = Random.Range(-xBoundary, xBoundary);
        float zPos = 0.0f;

        if (whichSide == 1)
            zPos = Random.Range(-zCloseBoundary, -zFarBoundary);
        else
            zPos = Random.Range(zCloseBoundary, zFarBoundary);

        Vector3 destination = new Vector3(xPos, 0.0f, zPos);

        return destination;
    }

    private void Update()
    {
        if(spawnTimer < Time.time)
        {
            spawnTimer = Mathf.Infinity; //Temporary Solution *TODO: FIX
            currentState = EnemyState.Null;
        }

        if (idleTimer != 0.0f)
        { //Timer is activate
            if (idleTimer < Time.time)
            { //Check to see if times up. Previously if statement will proc if times up.
                idleTimer = 0.0f;
            }
        }

        if (pursuePlayerTimer != 0.0f)
        {
            if (pursuePlayerTimer < Time.time)
            {
                pursuePlayerTimer = 0.0f;
                if (currentState != EnemyState.Spawn)
                    currentState = EnemyState.Follow;
            }
        }

        if (currentState == EnemyState.Idle && pursuePlayerTimer == 0.0f & idleTimer == 0.0f)
        { //This should return true if we aren't trying to follow the player, and not idling
            idleDestination = CalculateRandomDestination();
            currentState = EnemyState.Walking;
        }

        if (currentState == EnemyState.Vulnerable)
        {
            if (vulnerableTimer < Time.time)
            {
                ourMesh.material.color = defaultColor; //Reset him to normal
                vulnerableTimer = 0.0f;
                ourCol.isTrigger = false;
                if (Vector3.Distance(player.transform.position, transform.position) < 5)
                    pursuePlayerTimer = Time.time + pursuePlayerDelay;
                else
                    currentState = EnemyState.Idle;
            }
        }

        if(attackTimer != 0.0f)
        { //If the timer is active, let's watch it and see if we can attack the player
            if(attackTimer < Time.time)
            {
                if(player != null)
                {//Sound was persisting on death, this is to stop that. LAST MINUTE FIX
                    AttackPlayer();

                    if (playerAttackable)
                        attackTimer = Time.time + attackRate;
                    else
                        attackTimer = 0.0f;
                }
            }
        }
    }

    private void AdjustGrappledVariables(bool value)
    { //I was typing this over and over earlier so I created this function. May no longer be necessary
        rb.isKinematic = value;
        ourCol.isTrigger = value;
    }

    private void OnCollisionEnter(Collision collision)
    { //If we fall off, kill ourselves on the boundary below
        if (collision.gameObject.CompareTag("Boundary"))
            emScript.Death(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    { //Use a second trigger collider to check if we can attack the player or not
        if(other.gameObject.CompareTag("Player"))
        {
            if (currentState == EnemyState.Follow)
            { //Only attack if we have stayed nearby half of the attack duration
                playerAttackable = true;
                attackTimer = Time.time + (attackRate * 0.5f);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    { //Make sure we can't hit them if they are too far away
        if(other.gameObject.CompareTag("Player"))
        {
            if (currentState == EnemyState.Follow)
            { //Turn off the attack timer if we moved away
                playerAttackable = false;
                attackTimer = 0.0f;
            }
        }
    }

    private void AttackPlayer()
    {
        phScript.DealDamage(power);
        ourAudio.Play();
    }

    public void SetSide(int side)
    { //Call this function on instantiation or when we get grappled
        whichSide = side;
    }

    public int GetSide()
    { //Return to the caller which side of the map we are on.
        return whichSide;
    }

    public void HitByGrapple()
    {
        currentState = EnemyState.Grappled;
        AdjustGrappledVariables(true);
    }

    public void GotGrappled(Transform hook)
    { //We got caught by the hook, adjust some values
        currentState = EnemyState.Grappled;
        transform.SetParent(hook);
    }

    public void ReleasedFromGrapple(bool towed, int side)
    {
        transform.SetParent(null);
        AdjustGrappledVariables(false);

        if (towed)
        {
            SetSide(side);
            Vulnerable();
        }
        else
        { //This should reset out check for idle walking path and then proceed
            currentState = EnemyState.Idle;
            ourCol.isTrigger = false;
        }

    }

    public void Vulnerable()
    { //Set everything to let the player know we are feeling vulnerable
        currentState = EnemyState.Vulnerable;
        vulnerableTimer = Time.time + vulnerableTime;
        ourMesh.material.color = Color.white; //Change our color to let the player know he is vulnerable
    }

    public bool IsVulnerable()
    {
        bool areWe = false;
        if (currentState == EnemyState.Vulnerable)
            areWe = true;

        return areWe;
    }

    public void PlayerLeftSide()
    {
        if(currentState != EnemyState.Spawn)
            currentState = EnemyState.Idle;
    }

    public void PlayerJoinedSide()
    {
        if (currentState != EnemyState.Spawn)
            if (currentState != EnemyState.Vulnerable)
                pursuePlayerTimer = Time.time + pursuePlayerDelay;
    }
}