﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour 
{
    public float edgeBounds = 12.5f;
    public float followSpeed = 8.0f;
    public float cameraBend = 10.0f;

    public Vector3 cameraLook = new Vector3(0, 0, 0);

    private Transform lookTarget;
    private Vector3 offsetToTarget;

    private float baseFOV = 60;
    private float viewAlienFOV = 90;
    private float transitionSpeed = 0.25f;

    private float lookAtAlienTimeMin = 15.0f;
    private float lookAtAlienTimeMax = 30.0f;
    private float lookingAtAlienTime = 5.0f;

    public float lookAtAlienTimer = 0.0f;

    public bool transitionFOV = false;
    public bool lookingAtAlien = false;


	// Use this for initialization
	void Start () 
	{
        lookAtAlienTimer = Time.time + 5.0f + (Random.Range(lookAtAlienTimeMin, lookAtAlienTimeMax));

        lookTarget = GameObject.FindGameObjectWithTag("Player").transform;
        if (lookTarget == null)
            Application.Quit();
        else
            offsetToTarget = transform.position - lookTarget.position;
	}

    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, (lookTarget.position + offsetToTarget), followSpeed * Time.deltaTime);
        RotateCamera();
    }

    private void Update()
    {
        if(lookAtAlienTimer != 0.0f)
        {
            if(lookAtAlienTimer < Time.time)
            {
                if(!lookingAtAlien)
                {
                    lookAtAlienTimer = Time.time + lookingAtAlienTime;
                    transitionFOV = true;
                }
                else
                {
                    lookAtAlienTimer = 0.0f;
                    transitionFOV = true;
                }
            }
        }
        else
        {
            if (!transitionFOV || !lookingAtAlien)
                lookAtAlienTimer = Time.time + Random.Range(lookAtAlienTimeMin, lookAtAlienTimeMax);
        }

        if(transitionFOV)
        {
            if(lookingAtAlien)
            {
                Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, baseFOV, transitionSpeed * Time.deltaTime);

                if ((Camera.main.fieldOfView - baseFOV) <= 0.1f)
                {
                    transitionFOV = false;
                    lookingAtAlien = false;
                    Camera.main.fieldOfView = baseFOV;
                }
            }
            else
            {
                Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, viewAlienFOV, transitionSpeed * Time.deltaTime);

                if ((viewAlienFOV - Camera.main.fieldOfView) <= 0.1f)
                {
                    transitionFOV = false;
                    lookingAtAlien = true;
                    Camera.main.fieldOfView = viewAlienFOV;
                }
            }
        }
    }

    private void RotateCamera()
    {
        float horiPos = 0.0f;
        if (cameraLook.y == 0.0f)
            horiPos = -(transform.position.x / edgeBounds);
        else
            horiPos = transform.position.x / edgeBounds;

        Quaternion newRot = Quaternion.Euler(cameraLook.x, cameraLook.y + (cameraBend * horiPos), cameraLook.z);
        
        transform.rotation = newRot;
    }

    public void PlayerSwitchedSides()
    {
        if (cameraLook.y == 0.0f)
        {
            cameraLook.y += 180.0f;
            offsetToTarget.z += -30.0f;
        }
        else
        {
            cameraLook.y -= 180.0f;
            offsetToTarget.z += 30.0f;
        }
    }
}