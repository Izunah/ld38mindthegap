﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTileMover : MonoBehaviour 
{
    public bool descend = false;
    public bool ascend = false;

    private float speed = 5.0f;

    private void Update()
    {
        if(descend)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, 0.0f, transform.position.z), speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, new Vector3(transform.position.x, 0.0f, transform.position.z)) < 0.01)
                descend = false;
        }

        if(ascend)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, 27.5f, transform.position.z), speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, new Vector3(transform.position.x, 27.5f, transform.position.z)) < 0.01)
                ascend = false;
        }

        if(descend && ascend)
        { //Failsale, sometimes the platform was getting stuck, this prevents that from happening (SO FAR).
            descend = false;
        }
    }

    public void DescendTile()
    {
        descend = true;
    }

    public void AscendTile()
    {
        ascend = true;
    }

    public bool InUse()
    {
        bool inUse = false;
        if (descend || ascend)
            inUse = true;

        return inUse;
    }
}