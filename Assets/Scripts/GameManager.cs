﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour 
{
    public static bool GameStarted = false;
    public Animator canvasAC;

    public Text playerScore;
    public Text highScoreText;
    public Text comboText;
    public Text multiplierText;
    public Text getReadyText;
    public Slider healthSlider;

    public float multiplierIncrease = 0.25f;

    private float restartTime = 8.0f;
    private float restartTimer = 0.0f;

    private int currentScore = 0;
    private int highScore = 0;
    private int combo = 0;

    private float multiplier = 1.0f;

    private GameObject player;
    private bool playerDead = false;

    private void Awake()
    { //First time saving a games values, yay!
        if (PlayerPrefs.HasKey("Highscore"))
        {
            highScore = PlayerPrefs.GetInt("Highscore");
        }
        else
        {
            PlayerPrefs.SetInt("Highscore", highScore);
        }
    }

    private void Start()
    {
        UpdateUI();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if (GameStarted && !playerDead)
        {
            if (gameObject.GetComponent<EnemyManager>().enabled == false)
                gameObject.GetComponent<EnemyManager>().enabled = true;

            if(player.GetComponent<PlayerController>().enabled == false)
            {
                player.transform.SetParent(null);
                player.GetComponent<PlayerController>().enabled = true;
            }

            if (getReadyText.enabled == true)
                getReadyText.enabled = false;
        }

        if (restartTimer != 0.0f)
        {
            if (restartTimer < Time.time)
            {
                SceneManager.LoadScene(0);
            }
        }
    }

    public void AddScore(int score)
    { //Temporary to test scoring
        currentScore += Mathf.FloorToInt((float)score * ((combo + 10) / 10) * multiplier);
        combo += 1;

        CheckHighScore();
        UpdateUI();
    }

    public void ResetMultiplier()
    {
        multiplier = 1.0f;
    }

    public void AddToMultiplier(float effect)
    {
        multiplier += multiplierIncrease * effect;
        UpdateUI();
    }

    public void UpdateHealth(int value)
    {
        healthSlider.value = value;
    }

    public void PlayerDied()
    {
        //Do nothing yet, be we will need this.
        canvasAC.SetTrigger("GameOver");
        playerDead = true;
        GameStarted = false;
        restartTimer = Time.time + restartTime;
    }

    private void CheckHighScore()
    {
        if(highScore < currentScore)
        {
            highScore = currentScore;
            PlayerPrefs.SetInt("Highscore", highScore);
            PlayerPrefs.Save();
        }
    }

    private void UpdateUI()
    {
        playerScore.text = "Score: " + currentScore;
        highScoreText.text = "Highscore: " + highScore;
        comboText.text = "Combo:    " + combo;
        multiplierText.text = "Multiplier: " + multiplier.ToString("F2");
    }
}