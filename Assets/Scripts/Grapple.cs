﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grapple : MonoBehaviour 
{
    public GameObject grappleGO;
    public AudioClip clip_Grapple;
    public AudioClip clip_GrappleHit_wall;
    public AudioClip clip_GrappleHit_enemy;
    public AudioClip clip_PlayerJump;

    public float baseGrappleSpeed = 100.0f;
    public float grappleReturnSpeedModifier = 2.0f;
    public float grappleTowSpeedModifier = 0.5f; //How much we slow down our return of the grapple with an enemy/player on it.
    public float grappleTowTargetSlowModifier = 0.25f;

    public float releaseGrappleInSeconds = 2.0f;
    public float releaseGrappleMissModifier = 0.34f;

    public float fireDelay = 0.5f;

    private Vector3 grappleOffset;
    private Rigidbody grappleRb;
    private Collider grappleCollider;
    private LineRenderer grappleLine;

    private PlayerController pController;
    private AudioSource audioSource;

    private enum grappleState { Idle, Firing, Returning, Contact, Towing };
    private grappleState curGrappleState;

    private float currentGrappleSpeed = 100.0f;
    private float grappleDistance = 75.0f;
    private float fireDelayTimer = 0.0f;

    private int grappleReturnableDistance = 5;
    private int grappleTowReturnableDistance = 2;
    private int towTargetSlowThreshold = 7;

    private void Start()
    {
        pController = gameObject.GetComponentInParent<PlayerController>();
        audioSource = gameObject.GetComponent<AudioSource>();

        currentGrappleSpeed = baseGrappleSpeed;
        curGrappleState = grappleState.Idle;
        grappleLine = GetComponent<LineRenderer>();
        grappleLine.enabled = false; //Should be inactive in scene but this is a safety precaution.

        grappleOffset = grappleGO.transform.position - transform.position;
        grappleRb = grappleGO.GetComponent<Rigidbody>();
        grappleCollider = grappleGO.GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update () 
	{
        if (Input.GetButtonDown("Fire1") && curGrappleState == grappleState.Idle && fireDelayTimer < Time.time)
        {
            StartCoroutine(FireGrapple());
        }
	}

    private void FixedUpdate()
    {
        //Handle the movement of the sphere in fixed update so we can handle collisions properly.
        if (curGrappleState == grappleState.Firing)
        {
            grappleRb.MovePosition(grappleGO.transform.position + (transform.parent.forward * currentGrappleSpeed * Time.deltaTime)); //Fire out in the direction of the player character
        }
        if(curGrappleState == grappleState.Returning || curGrappleState == grappleState.Towing)
        {
            Vector3 returnDir = (transform.position + grappleOffset) - grappleGO.transform.position; //Calculate the angle vector we need to return the hook towards.
            grappleRb.MovePosition(grappleGO.transform.position + (returnDir.normalized * currentGrappleSpeed * Time.deltaTime)); //Move towards our starting location.
        }
    }

    private void LateUpdate()
    {
        UpdateGrappleLine(); //Handle this is late udpate so the line renderer tracks properly
    }

    IEnumerator FireGrapple()
    {
        currentGrappleSpeed = baseGrappleSpeed; //Zero out any modifiers we may have added
        pController.GrappleFired(); //Call the fired method on the controller
        curGrappleState = grappleState.Firing;

        grappleGO.transform.SetParent(null);
        grappleGO.GetComponent<Collider>().isTrigger = false;
        grappleRb.isKinematic = false;

        grappleCollider.isTrigger = false; //Make sure we aren't a trigger so we have a collision with a random object

        grappleLine.enabled = true; //Enable our line renderer

        Vector3 grappleOrigin = grappleGO.transform.position;
        audioSource.clip = clip_Grapple;
        audioSource.timeSamples = 1;
        audioSource.pitch = 1.0f;
        audioSource.Play();
        while(Vector3.Distance(grappleOrigin, grappleGO.transform.position) < grappleDistance)
        {
            yield return null; //Wait until fixed update has pushed us past our grapple distance, collision will end this coroutine.
        }

        StartCoroutine(ReturnGrapple(false)); //If we didn't hit anything at all, just turn.
    }

    IEnumerator ReturnGrapple(bool isTowing, bool playSound = true, GameObject obj = null) //If we have a player in tow, we need to activate the bool.
        //TODO: How we we tell if the player is going to the grapple? Probably have to have another coroutine to handle that, since that won't be the sphere moving.
    {
        grappleCollider.isTrigger = true; //Set the collider trigger so we don't knock our player around on an unexpected collsion

        if(playSound)
        {
            audioSource.clip = clip_Grapple;
            audioSource.time = 0.350f;
            audioSource.pitch = -1;
            audioSource.Play();
        }

        if (isTowing)
        {
            if (obj == null)
                Application.Quit();

            curGrappleState = grappleState.Towing;
            currentGrappleSpeed = baseGrappleSpeed * grappleTowSpeedModifier; //If we tow, use the tow modifier to slow things down a bit.
            StartCoroutine(GrappleTowing(obj));
        }
        else
        {
            if(obj != null)
            {
                obj.GetComponent<EnemyBehaviour>().ReleasedFromGrapple(false, pController.GetSide()); //Tell the object we have brought him to his destination
            }

            curGrappleState = grappleState.Returning;
            currentGrappleSpeed = baseGrappleSpeed * grappleReturnSpeedModifier; //If we aren't towing, return rapidly.
            StartCoroutine(GrappleReturning());
        }

        while (curGrappleState != grappleState.Idle)
            yield return null;

        grappleLine.enabled = false;
        grappleRb.isKinematic = true; //Turn off the hooks rigidbody
        grappleGO.transform.SetParent(transform); //Re-parent us to follow the player
        grappleGO.transform.position = transform.position + grappleOffset; //Offset us so we are back where we started
        grappleGO.GetComponent<Collider>().isTrigger = true;
        pController.GrappleReturned(); //Tell the player the grapple hook has returned, this resets his speed
        fireDelayTimer = Time.time + fireDelay;
    }

    IEnumerator GrappleReturning()
    {
        while (Vector3.Distance(grappleGO.transform.position, transform.position) > grappleReturnableDistance)
            yield return null;

        curGrappleState = grappleState.Idle;
    }

    IEnumerator GrappleTowing(GameObject obj)
    {
        while (Vector3.Distance(grappleGO.transform.position, transform.position) > grappleTowReturnableDistance) //How close are we to returning home? Grab us in a reasonable not game breaking distance
        {
            /*while (currentGrappleSpeed == baseGrappleSpeed * grappleTowSpeedModifier) Disabled temporary if I decide to give a tow slow when the enemy approaches the player
            {
                if (Vector3.Distance(grappleGO.transform.position, transform.position) < towTargetSlowThreshold)
                {
                    currentGrappleSpeed *= grappleTowTargetSlowModifier;
                    break;
                }
                yield return null;
            }*/
            yield return null;
        }

        curGrappleState = grappleState.Idle;
        pController.TowedEnemy(obj); //Tell the player we towed an enemy near to them.
        obj.GetComponent<EnemyBehaviour>().ReleasedFromGrapple(true, pController.GetSide()); //Tell the object we have brought him to his destinatio
    }

    IEnumerator HandleGrapple(Collision col)
    {
        curGrappleState = grappleState.Contact; //Tell all the other commands we have hit something!
        float grappleReleaseTimer = 0.0f; //Set up the timer to check how long we have been grappling.
        EnemyBehaviour ebScript = null;

        //Set our release timer based on whether we collided with an enemy, or a non-enemy
        if (col.gameObject.CompareTag("Enemy"))
        {
            audioSource.clip = clip_GrappleHit_enemy;
            audioSource.Play();

            ebScript = col.gameObject.GetComponent<EnemyBehaviour>();
            if(ebScript.GetSide() == pController.GetSide())
            {
                StopAllCoroutines();
                StartCoroutine(ReturnGrapple(false));
            }
            else
                ebScript.HitByGrapple();

            grappleReleaseTimer = Time.time + releaseGrappleInSeconds; //Set up the timer to check how long we have been grappling.
        }
        else
        {
            grappleReleaseTimer = Time.time + (releaseGrappleInSeconds * releaseGrappleMissModifier); //Set up the timer to check how long we have been grappling.
            audioSource.clip = clip_GrappleHit_wall;
            audioSource.Play();
        }

        //Wait until the timer ticks down, and check for any player input incase they wish to do something while grappled
        while (grappleReleaseTimer > Time.time)
        {
            if(col.gameObject.CompareTag("Enemy"))
            {
                //If we hit fire1 a.k.a left mouse button, pull the enemy towards us
                if (Input.GetButtonDown("Fire1"))
                {
                    ebScript.GotGrappled(grappleGO.transform);
                    col.gameObject.transform.SetParent(grappleGO.transform); //We will pull the object along with out hook.
                    col.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                    StopAllCoroutines(); //Kill this coroutine, we don't need it anymore. It should not kill any other coroutines, will need to re-assess with this is an issue
                    StartCoroutine(ReturnGrapple(true, true, col.gameObject)); //Tell the return grapple function we have an enemy with us
                }
                if(Input.GetButtonDown("Fire2"))
                {
                    //Leap to the enemy.
                    audioSource.clip = clip_PlayerJump;
                    audioSource.Play();
                    StopAllCoroutines();
                    pController.StartCoroutine(pController.CrossSide(col.gameObject.transform));
                }
            }
            yield return null;
        }

        if (col.gameObject.CompareTag("Enemy"))
            StartCoroutine(ReturnGrapple(false, true, col.gameObject)); //Either we couldn't do anything with our hook or decided the timing was bad, return the hook.
        else
            StartCoroutine(ReturnGrapple(false));
    }

    private void UpdateGrappleLine() //Called in late update to keep the line synced nicely with our movement.
    {
        if(grappleLine.enabled)
        {
            grappleLine.SetPosition(0, transform.position);
            grappleLine.SetPosition(1, grappleGO.transform.position);
        }
    }
    
    public void Hit(Collision col)
    {
        StopAllCoroutines(); //Kill anything we have on the go, as we will start a new routine.
        StartCoroutine(HandleGrapple(col)); //Pass our collision, we need some information about it.
    }

    public void PlayerCrossed()
    {
        StartCoroutine(ReturnGrapple(false,false));
    }
}