﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject[] s1SpawnLocations; //Side 1 Spawns
    public GameObject[] s2SpawnLocations; //Side 2 Spawns
    public GameObject enemyPrefab;
    public GameObject enemyDeathParticles;

    //Spawn Times and Timers
    public float spawnOppositeDelay = 5.0f;
    public float spawnSameDelay = 8.0f;

    private float spawnOppositeTimer = 0.0f;
    private float spawnSameTimer = 0.0f;

    private PlayerController pcScript;
    private GameManager gmScript;

    private void Start()
    {
        pcScript = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        gmScript = gameObject.GetComponent<GameManager>(); //This script is on the same object as gameManager, tweak this is it gets moved.

        spawnOppositeTimer = Time.time + spawnOppositeDelay;
        spawnSameTimer = Time.time + spawnSameDelay;
    }

    private void Update()
    {
        if(spawnOppositeTimer < Time.time)
        {
            SpawnOpposite();
        }
        
        if(spawnSameTimer < Time.time)
        {
            SpawnSame();
        }
    }

    public void PlayerSwappedSides(int side)
    {//We are calling the gmScript here because we already have the reference here. Not proper, but it works

        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject enemy in enemies)
        {
            EnemyBehaviour ebScript = enemy.GetComponent<EnemyBehaviour>();

            if (ebScript.currentState == EnemyBehaviour.EnemyState.Spawn)
                continue;

            if (ebScript.GetSide() == side)
            {
                ebScript.PlayerJoinedSide();
            }
            else
                ebScript.PlayerLeftSide();
        }

        //Force the opposite spawn to go off to keep a proper countdown, also an extra penalty for jumping(or boon depending how you look at it)
        spawnOppositeTimer = Mathf.NegativeInfinity;
        gmScript.ResetMultiplier();
    }

    public void PlayerDied()
    {
        gmScript.PlayerDied();
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        spawnOppositeTimer = Mathf.Infinity;
        spawnSameTimer = Mathf.Infinity;

        foreach(GameObject enemy in enemies)
        {
            enemy.GetComponent<EnemyBehaviour>().currentState = EnemyBehaviour.EnemyState.Idle;
        }
    }

    public void SpawnOpposite()
    {
        if(pcScript.GetSide() == 1)
        {
            SpawnAtSpawner(s2SpawnLocations, 2);
        }
        else
        {
            SpawnAtSpawner(s1SpawnLocations, 1);
        }

        spawnOppositeTimer = Time.time + spawnOppositeDelay;
    }

    public void SpawnAtSpawner(GameObject[] spawns, int side)
    {
        int spawner = Random.Range(0, spawns.Length);
        while(spawns[spawner].GetComponent<SpawnTileMover>().InUse())
        {
            spawner = Random.Range(0, spawns.Length);
        }

        Vector3 spawnPos = spawns[spawner].transform.position;
        spawnPos.y += 2.0f;
        GameObject enemy = Instantiate(enemyPrefab, spawnPos, spawns[spawner].transform.rotation, null) as GameObject;
        enemy.transform.SetParent(spawns[spawner].transform, true);

        spawns[spawner].GetComponent<SpawnTileMover>().DescendTile();

        EnemyBehaviour ebScript = enemy.GetComponent<EnemyBehaviour>();
        ebScript.enabled = true;
        ebScript.SetSide(side);
        ebScript.StartCoroutine(ebScript.SpawnInWorld());
    }

    public void SpawnSame()
    { //We are calling the gmScript here because we already have the reference here. Not proper, but it works
        if (pcScript.GetSide() == 1)
        {
            SpawnAtSpawner(s1SpawnLocations, 1);
        }
        else
        {
            SpawnAtSpawner(s2SpawnLocations, 2);
        }

        gmScript.AddToMultiplier(1.0f);
        spawnSameTimer = Time.time + spawnSameDelay;
    }

    public void Death(GameObject obj)
    {
        Instantiate(enemyDeathParticles, obj.transform.position, Quaternion.identity);
        gmScript.AddScore(obj.GetComponent<EnemyBehaviour>().score);
        Destroy(obj);
    }
}