﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleCollision : MonoBehaviour 
{
    public GameObject wallHitParticles;
    public GameObject enemyHitParticles;

    private Grapple grappleScript;

    private void Start()
    {
        grappleScript = GetComponentInParent<Grapple>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(LayerMask.LayerToName(collision.gameObject.layer) == "Grapple")
        {
            ContactPoint contact = collision.contacts[0];

            if (!collision.gameObject.CompareTag("Enemy"))
                Instantiate(wallHitParticles, contact.point, Quaternion.identity);
            else
                Instantiate(enemyHitParticles, contact.point, Quaternion.identity);

            grappleScript.Hit(collision);
        }
    }
}
